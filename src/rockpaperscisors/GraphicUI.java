/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rockpaperscisors;

import rockpaperscisors.RockPaperScisors.Element;

/**
 *
 * @author Bruno Severo Bataglin
 */
public class GraphicUI {

    private static final String[] RockRightHand = 
    {
        "    _______  ",
        "---'   ____) ",
        "      (_____)",
        "      (_____)",
        "      (____) ",
        "---.__(___)  "
    };

    private  static final String[] PaperRightHand =
    {
        "    _______       ",
        "---'   ____)____  ",
        "          ______) ",
        "          _______)",
        "         _______) ",
        "---.__________)   "
    };

    private  static final String[] ScissorsRightHand = 
    {
        "    _______       ",
        "---'   ____)____  ",
        "          ______) ",
        "          _______)",
        "      (____)      ",
        "---.__(___)       "
    };

    private static  final String[] RockLeftHand = 
    {
        "  _______    ",
        " (____   '---",
        "(_____)      ",
        "(_____)      ",
        " (____)      ",
        "  (___)__.---"
    };

    private static  final String[] PaperLeftHand = 
    {
        "      ________    ",
        "  ___(____    '---",
        " (______          ",
        "(_______          ",
        "  (_______        ",
        "    (_________.---"
    };

    private  static final String[] ScissorsLeftHand = 
    {
        "      _______     ",
        "  ___(____    '---",
        " (______          ",
        "(_______          ",
        "      (____)      ",
        "       (___)__.---"
    };
    
    public static String[] generateRightHand(Element element)
    {
        switch(element)
        {
            case Rock:
                return RockRightHand;
            case Paper:
                return PaperRightHand;
            case Scissor:
                return ScissorsRightHand;
        }
        return new String[0];
    }
    
    
    public static String[] generatLeftHand(Element element)
    {
        switch(element)
        {
            case Rock:
                return RockLeftHand;
            case Paper:
                return PaperLeftHand;
            case Scissor:
                return ScissorsLeftHand;
        }
        return new String[0];
    }

}
