package rockpaperscisors;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Bruno Severo Bataglin
 */
public class RockPaperScisors {

    public enum Element {
        Rock(3), Scissor(2), Paper(1);

        private final int value;

        Element(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Element getValueOf(int value) {
            for (Element element : Element.values()) {
                if (element.getValue() == value) {
                    return element;
                }
            }
            return Paper;
        }
    }

    public void MainLoop() {
        Scanner sc = new Scanner(System.in);
        int userChoice;

        do {
            try {
                printChoices();
                userChoice = sc.nextInt();
                if (userChoice == 0) {
                    break;
                }

                if (userChoice > 0 && userChoice < 4) {
                    checkResult(Element.getValueOf(userChoice), Element.getValueOf(RandomNumber()));
                    System.out.println("\n");
                } else {
                    System.out.println("\nInvalid input!\n");
                }
            } catch (Exception e) {
                System.out.println("Invalid input!\n");
                sc.next();
            }
        } while (true);
    }

    private void checkResult(Element userChoice, Element computerChoice) {
        String result;
        printBattle(userChoice, computerChoice);

        if (userChoice == computerChoice) {
            result = "Draw match";
        } else if ((userChoice.equals(Element.Rock) && computerChoice.equals(Element.Scissor))
                || (userChoice.equals(Element.Scissor) && computerChoice.equals(Element.Paper))
                || (userChoice.equals(Element.Paper) && computerChoice.equals(Element.Rock))) {
            result = "Player wins";
        } else {
            result = "Computer wins";
        }

        System.out.println(result);
    }

    private void printChoices() {
        System.out.println("Choose between ");

        for (int i = 1; i < 4; i++) {
            System.out.println(String.format("%d - %s", i, Element.getValueOf(i)));
        }
        System.out.println("Type 0 to exit.");
    }

    private void printBattle(Element userChoice, Element computerChoice) {
        String[] user = GraphicUI.generateRightHand(userChoice);
        String[] computer = GraphicUI.generatLeftHand(computerChoice);

        System.out.println(String.format("\n%-20s   %-10s   %-20s", "PLAYER", "", "COMPUTER"));

        for (int i = 0; i < 6; i++) {
            System.out.println(String.format("%-20s   %-10s   %-20s", user[i], i == 3 ? "VS" : "", computer[i]));
        }
        System.out.println("\n");
    }

    private int RandomNumber() {
        return new Random().nextInt(3) + 1;
    }
}
